//1. Two Sum

class Solution {

    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {

        for i in 0..<nums.count{

            for j in i+1..<nums.count{

                if nums[i] + nums[j] == target{

                    

                    return [i,j]

                }

            }

        }

        return[0,0]

    }

}


let nums: [Int] = [2, 1, 7, 15]

let target = 9

var object = Solution()

var result = object.twoSum(nums, target)
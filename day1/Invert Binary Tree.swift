class Solution {
    func invertTree(_ root: TreeNode?) -> TreeNode? {
        if let tree = root{
            var left: TreeNode? = invertTree(tree.left)
            var right: TreeNode? = invertTree(tree.right)
            tree.left=right
            tree.right=left
            return tree
        }
        
        return root
        
    }
}